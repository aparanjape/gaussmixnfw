# README #

Code implementing analytical calculations of spherically averaged tidal fields in various cosmic web environments as described in [arXiv:2006.13954](https://arxiv.org/abs/2006.13954).

## Requirements ##

Python2.7 with [NumPy](https://numpy.org), [SciPy](https://www.scipy.org), optionally [Matplotlib](https://matplotlib.org) and [Jupyter](https://jupyter.org) (for example usage)

## Description ##

**gaussmix_nfw.py** contains two classes --

* _GaussMixNFW_ : routines that invoke non-negative least squares to fit a Gaussian mixture to spherically symmetric profiles, currently implemented for [NFW](https://ui.adsabs.harvard.edu/abs/1996ApJ...462..563N/abstract) profiles given values of virial radius ```Rvir``` (in Mpc/h) and concentration ```cvir```.

* _AnisoIntegrals_ : routines to calculate integrals defining spherically averaged overdensity and tidal anisotropy for various toy examples, currently restricted to axisymmetric situations.

## Examples ##

The Jupyter notebook **AnisoNFW_examples.ipynb** contains example usage and code to reproduce all the main plots of [arXiv:2006.13954](https://arxiv.org/abs/2006.13954) (needs Matplotlib). 

The folder **img/** contains example plots of the NFW profile and tidal anisotropy for the cosmic web configurations discussed in [arXiv:2006.13954](https://arxiv.org/abs/2006.13954).

The code snippet below demonstrates a simple example of creating and plotting the Gaussian mixture for a single spherical NFW halo.

## 

	import numpy as np
	import matplotlib.pyplot as plt
	from gaussmix_nfw import GaussMixNFW
	
	Rvir = 1.0 # virial radius in Mpc/h
	cvir = 7.0 # NFW halo concentration
	rforce = 2.5 # force resolution in kpc/h, controls dynamic range of fit
	
	gm = GaussMixNFW(Rvir=Rvir,cvir=cvir,rforce=rforce)
	
	# gm.WJ and gm.CJ now respectively contain the weights and Gaussian widths of gm.NCOMP components. 
	# gm.rs contains the NFW scale radius Rvir/cvir in Mpc/h
	# gm.nfwGM(x) [ gm.nfw(x) ] give the Gaussian mixture [actual] profile at x=r/gm.rs, normalised to unit enclosed overdensity inside r < Rvir
	# gm.gauss(x,cj) calculates the Gaussian component with width cj, normalised to unit enclosed overdensity inside r < Rvir
	# gm.JVALS contains indexes of the Gaussian components that will be used in any calculation. 
	#          This is initialised to np.arange(gm.NCOMP) but can be altered by the user.
	#          This is useful in modelling truncation of the profile, e.g., due to satellite mass loss (see Jupyter notebook for an example).

	xv = np.logspace(np.log10(0.0025*cvir),np.log10(15*cvir),200) # range of values for r/rs
	
	plt.xlim([0.5*xv.min()/gm.cvir,2*xv.max()/gm.cvir])
	plt.ylim([2e-5,2e4])        
	plt.xlabel("$r \,/\, R_{{\\rm vir}}$")
	plt.ylabel("$\\Delta(r) \,/\, \\Delta(<R_{{\\rm vir}})$")
	plt.xscale('log')
	plt.yscale('log')

	plt.plot(xv/gm.cvir,gm.nfw(xv),'-',color='teal',label="NFW",lw=1.0)
	plt.plot(xv/gm.cvir,gm.nfwGM(xv),'-',color='firebrick',label="GM: {0:d}comp".format(gm.JVALS.size),lw=1.5)
	for j in gm.JVALS:
    	plt.plot(xv/gm.cvir,gm.WJ[j]*gm.gauss(xv,gm.CJ[j]),'--',color='navy',lw=0.4)


	plt.legend(loc='upper right')
	plt.show()

![example](img/example-nfw.png)

## Credit ##

Paranjape (2020), [arXiv:2006.13954](https://arxiv.org/abs/2006.13954)

## Contact ##

Aseem Paranjape
